//sleep function provided by <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){

	printf("Sleeping for 5 seconds \n");
	sleep(5);
	printf("Wake up \n");
}
