// P.D.E INSTITUTO SAN JOSE A-355
// ING. GABRIEL GARCIA
// EJERCICIO TOKEN: APERTURA DE ARCHIVOS

//abrir el archivo listado.txt y mostrar su contenido en pantalla

#include <stdio.h>
#include <stdlib.h>

main()
{
	FILE *fichero;
	char letra;

	fichero = fopen("5ELCA.TXT","w");
	if (fichero == NULL) {
		printf( "No se puede abrir el fichero.\n" );
		exit( 1 );
	}
 //ESCRIBO EN EL ARCHIVO, LOS NOMBRES DE LOS ALUMNOS
 //TENER EN CONSIDERACION QUE EL PETODO DE APERTURA ES A+
 //POR LO QUE ESTAR� SUMANDO CONTENIDO (APPEND) A ARCHIVO EXISTENTE

	printf( "Contenido del fichero:\n" );
	letra = getc(fichero);
	while (feof(fichero) == 0) {
		printf( "%c",letra );
		letra = getc(fichero);
	}
 fprintf (fichero,"\nMATILDE, PILATE");                     //ALUMNO 1
 fprintf (fichero,"\nLUCAS, PATO");                         //ALUMNO 2
 fprintf (fichero,"\nMARTA, TUADA");                        //ALUMNO 3
 fprintf (fichero,"\nCESAR, NOSO");                         //ALUMNO 3

 	if (fclose(fichero)!= 0)
		printf( "Problemas al cerrar el fichero\n" );
}
