/*archivo controlbits.c

Para compilara el programa
gcc -o controlbits controlbits.c
*/

#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>

struct termios tio;

int main(int argc, char *argv[])
{
int fd;
int status;

if (argc != 4)
   {

   printf(�Para ejecutar el programa  controlbits /dev/ttyS0 \n�);

   printf(�Para ejecutar el programa  controlbits /dev/ttyUSB0 \n�);
  return 1 ;
   }

if ((fd = open(argv[1],O_RDWR)) < 0)
   {
    printf(�No se puede abri el perto \n�,argv[1]);
   return 1;
   }
tcgetattr(fd, &tio);                           /*Consigue informacion del puerto*/
tio.c_cflag &= ~HUPCL;                /* Resetea el bit HUPCL */
tcsetattr(fd, TCSANOW, &tio);  /* set the termio information */

ioctl(fd, TIOCMGET, &status); /* Consigue el status del puerto serie */

if ( argv[2][0] == �1� )      /* Pon a uno la linea  DTR  */
           status &= ~TIOCM_DTR;
      else
          status |= TIOCM_DTR; /* Si no a cero  */

if ( argv[3][0] == �1� )      /* pon a uno la linea RTS */
          status &= ~TIOCM_RTS;
     else
          status |= TIOCM_RTS;  /* Si no a cero  */

ioctl(fd, TIOCMSET, &status); /* Varia al nuevo status las lineas del puerto serie */

close(fd);                    /* Cierra el puerto serie */
}
